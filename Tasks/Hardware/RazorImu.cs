﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.AllJoyn;
using Windows.Devices.SerialCommunication;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;

namespace Tasks.Hardware
{
    internal class RazorImu
    {
        private SerialDevice _razorSerialDevice;
        private DataReader _razorInputStream;
        private DataWriter _razorOutputStream;

        private bool _imuInit;
        private double _yaw;
        private double _roll;
        private double _pitch;
        private double _accelX;
        private double _accelY;
        private double _accelZ;
        private double _magX;
        private double _magY;
        private double _magZ;

        private double _gyroX;
        private double _gyroY;
        private double _gyroZ;

        private static string _lastImuData;

        internal delegate void ImuEventHandler(ImuDataArgs args);

        internal event ImuEventHandler ImuEvent;

        private Stopwatch _stopwatch;

        internal RazorImu()
        {

        }

        internal async Task<bool> InitializeAsync()
        {
            _razorSerialDevice = await ImuBackgroundTask.SerialDeviceHelper.GetSerialDeviceAsync("A504WXPUA", 57600, TimeSpan.FromMilliseconds(25), TimeSpan.FromMilliseconds(25));

            if (_razorSerialDevice == null)
                return false;

            _razorInputStream = new DataReader(_razorSerialDevice.InputStream) { InputStreamOptions = InputStreamOptions.Partial };
            _razorOutputStream = new DataWriter(_razorSerialDevice.OutputStream);

            return true;
        }

        internal async Task StartAsync()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();

            while (true)
            {
                if (_razorInputStream == null || _razorOutputStream == null)
                    await Task.Delay(500);

                await ReadImu();
            }
        }

        internal async Task ReadImu()
        {
            if (!_imuInit)
            {
                _imuInit = true;
                await Task.Delay(3000); //Let the IMU finish booting up
            }

            _razorOutputStream.WriteBytes(Encoding.ASCII.GetBytes($"#ot{'\r'}{'\n'}#f{'\r'}{'\n'}")); //Ask for YPR
            await _razorOutputStream.StoreAsync().AsTask();

            var dataCount = 0;
            var incomingBytes = new List<byte> { 0x00 };

            while (dataCount < 4)
            {
                await _razorInputStream.LoadAsync(1).AsTask();
                var inChar = _razorInputStream.ReadByte();

                incomingBytes.Add(inChar);

                if (inChar == 0x0a)
                    dataCount++;

                if (dataCount != 1 || inChar != 0x0a)
                    continue;

                _razorOutputStream.WriteBytes(Encoding.ASCII.GetBytes($"#osct{'\r'}{'\n'}#f{'\r'}{'\n'}")); //Ask for All raw data
                await _razorOutputStream.StoreAsync().AsTask();
            }

            var imuData = Encoding.ASCII.GetString(incomingBytes.ToArray()).Replace("\0", "").Replace("\r", "").Replace("\n", "");

            if (!string.IsNullOrEmpty(imuData))
                ParseImu(imuData);

            if (_stopwatch.ElapsedMilliseconds <= 40)
                return;

            ImuEvent?.Invoke(new ImuDataArgs { Yaw = _yaw, Pitch = _pitch, Roll = _roll });
            _stopwatch.Restart();

            //await _display.WriteAsync($"Yaw {_yaw}");
        }

        internal async Task<ImuDataArgs> GetImuData()
        {
            if(!_imuInit)
            {
                _imuInit = true;
                await Task.Delay(3000);
            }

            _razorOutputStream.WriteBytes(Encoding.ASCII.GetBytes($"#ot{'\r'}{'\n'}#f{'\r'}{'\n'}")); //Ask for YPR
            await _razorOutputStream.StoreAsync().AsTask();

            var dataCount = 0;
            var incomingBytes = new List<byte> { 0x00 };

            while (dataCount < 4)
            {
                await _razorInputStream.LoadAsync(1).AsTask();
                var inChar = _razorInputStream.ReadByte();

                incomingBytes.Add(inChar);

                if (inChar == 0x0a)
                    dataCount++;

                if (dataCount != 1 || inChar != 0x0a)
                    continue;

                _razorOutputStream.WriteBytes(Encoding.ASCII.GetBytes($"#osct{'\r'}{'\n'}#f{'\r'}{'\n'}")); //Ask for All raw data
                await _razorOutputStream.StoreAsync().AsTask();
            }

            var imuData = Encoding.ASCII.GetString(incomingBytes.ToArray()).Replace("\0", "").Replace("\r", "").Replace("\n", "");

            if(!string.IsNullOrEmpty(imuData))
            {
                ParseImu(imuData);
            }
        
            return new ImuDataArgs { Yaw = _yaw, Pitch = _pitch, Roll = _roll };
        }

        internal void ParseImu(string imuData) //Probably a better way...
        {
            try
            {
                foreach (var data in imuData.Split('#'))
                {
                    if (string.IsNullOrEmpty(data))
                        continue;

                    if (data.Contains("YPR"))
                    {
                        var splitData = data.Replace("YPR=", "").Split(',');

                        if (splitData.Length >= 1)
                        {
                            var yaw = 0d;
                            double.TryParse(splitData[0], out yaw);

                            _yaw = yaw;
                        }

                        if (splitData.Length >= 2)
                            double.TryParse(splitData[1], out _pitch);

                        if (splitData.Length >= 3)
                            double.TryParse(splitData[2], out _roll);

                        continue;
                    }

                    if (data.Contains("A-C="))
                    {
                        var splitData = data.Replace("A-C=", "").Split(',');

                        if (splitData.Length >= 1)
                            double.TryParse(splitData[0], out _accelX);

                        if (splitData.Length >= 2)
                            double.TryParse(splitData[1], out _accelY);

                        if (splitData.Length >= 3)
                            double.TryParse(splitData[2], out _accelZ);

                        continue;
                    }

                    if (data.Contains("M-C="))
                    {
                        var splitData = data.Replace("M-C=", "").Split(',');

                        if (splitData.Length >= 1)
                            double.TryParse(splitData[0], out _magX);

                        if (splitData.Length >= 2)
                            double.TryParse(splitData[1], out _magY);

                        if (splitData.Length >= 3)
                            double.TryParse(splitData[2], out _magZ);

                        continue;
                    }

                    if (data.Contains("G-C="))
                    {
                        var splitData = data.Replace("G-C=", "").Split(',');

                        if (splitData.Length >= 1)
                            double.TryParse(splitData[0], out _gyroX);

                        if (splitData.Length >= 2)
                            double.TryParse(splitData[1], out _gyroY);

                        if (splitData.Length >= 3)
                            double.TryParse(splitData[2], out _gyroZ);
                    }
                }
            }
            catch
            {
                //
            }

            _lastImuData = $"{_accelX},{_accelY},{_accelZ},{_gyroX},{_gyroY},{_gyroZ},{_magX},{_magY},{_magZ},{_yaw},{_pitch},{_roll}";
        }


    }

    internal class ImuDataArgs : EventArgs
    {
        internal double Yaw { get; set; }
        internal double Pitch { get; set; }
        internal double Roll { get; set; }
    }
}

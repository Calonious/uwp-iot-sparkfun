﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.AppService;
using Windows.ApplicationModel.Background;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Tasks.Hardware;

// ReSharper disable once CheckNamespace
namespace Tasks
{
    public sealed class ImuBackgroundTask : IBackgroundTask
    {
        private BackgroundTaskDeferral _deferral;
        internal static readonly SerialDeviceHelper SerialDeviceHelper = new SerialDeviceHelper();
        private readonly List<Task> initializeTasks = new List<Task>();
        private readonly List<Task> startupTasks = new List<Task>();

        ThreadPoolTimer _periodicTimer = null;

        private AppServiceConnection _appServiceConnection;

        private RazorImu _razorImu;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            _deferral = taskInstance.GetDeferral();
            taskInstance.Canceled += OnTaskCanceled;

            AppServiceTriggerDetails details = taskInstance.TriggerDetails as AppServiceTriggerDetails;

            if (details != null)
            {
                _appServiceConnection = details.AppServiceConnection;
            }
            if(_appServiceConnection != null)
                _appServiceConnection.RequestReceived += OnRequestReceived;

            _razorImu = new RazorImu();
            _razorImu.ImuEvent += RazorImuOnImuEvent;

            initializeTasks.Add(_razorImu.InitializeAsync());
            startupTasks.Add(_razorImu.StartAsync());

            // 
            // TODO: Insert code to perform background work
            //
            // If you start any asynchronous methods here, prevent the task
            // from closing prematurely by using BackgroundTaskDeferral as
            // described in http://aka.ms/backgroundtaskdeferral
            //

            // When we are done with the background tasks and async methods, close the deferral.

            await Task.WhenAll(initializeTasks);
	        await Task.WhenAll(startupTasks);

            _deferral.Complete();
        }

        private async void OnRequestReceived(AppServiceConnection sender, AppServiceRequestReceivedEventArgs args)
        {
            AppServiceDeferral messageDeferral = args.GetDeferral();
            ValueSet message = args.Request.Message;
            ValueSet returnMessage = new ValueSet();
            ImuDataArgs data = null;

            object act;
            message.TryGetValue("action", out act);

            try
            {
                switch ((string)act)
                {
                    case "getimudata":
                        data = await _razorImu.GetImuData();
                        returnMessage.Add("yaw", data.Yaw);
                        returnMessage.Add("pitch", data.Pitch);
                        returnMessage.Add("roll", data.Roll);
                        break;
                }
            }
            catch (Exception ex)
            {
                returnMessage.Add("status", ex.Message);
            }

            await args.Request.SendResponseAsync(returnMessage);
            messageDeferral.Complete();
        }

        private void OnTaskCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            _deferral?.Complete();
        }

        private void RazorImuOnImuEvent(ImuDataArgs args)
        {

        }
    }
}

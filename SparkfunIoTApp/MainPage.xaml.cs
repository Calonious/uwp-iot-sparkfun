﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.AppService;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SparkfunIoTApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ApplicationTrigger trigger = null;
        AppServiceConnection appConnection;

        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            trigger = new ApplicationTrigger();
            //RegisterBackgroundTask();
        }

        private void RegisterBackgroundTask()
        {
            var task = SparkfunIoTSample.RegisterBackgroundTask(SparkfunIoTSample.SampleBackgroundTaskEntryPoint, SparkfunIoTSample.SampleBackgroundTaskName, trigger, null);

            AttachProgressAndCompleteHandlers(task);
            UpdateUI();
        }

        private void UpdateUI(AppServiceResponse appServiceResponse = null)
        {   
            if(appServiceResponse == null)
			{
				YawTextBlock.Text = "Yaw: " + 15;
				RollTextBlock.Text = "Roll: " + 10;
				PitchTextBlock.Text = "Pitch: " + 10;
            }
            else
            {
                object yaw;
                object pitch;
                object roll;

                appServiceResponse.Message.TryGetValue("yaw", out yaw);
                appServiceResponse.Message.TryGetValue("pitch", out pitch);
                appServiceResponse.Message.TryGetValue("roll", out roll);
                
                YawTextBlock.Text = (string)yaw ?? string.Empty;
                RollTextBlock.Text = (string)roll ?? string.Empty;
                PitchTextBlock.Text = (string)pitch ?? string.Empty;
            }
        }

        private void AttachProgressAndCompleteHandlers(BackgroundTaskRegistration task)
        {
            task.Progress += OnProgress;
            task.Completed += OnCompleted;
        }

        private void OnCompleted(BackgroundTaskRegistration sender, BackgroundTaskCompletedEventArgs args)
        {
        }

        private void OnProgress(BackgroundTaskRegistration sender, BackgroundTaskProgressEventArgs args)
        {
        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            appConnection = new AppServiceConnection
            {
                AppServiceName = "SparkfunIoT",
                PackageFamilyName = "Test1-uwp_467drpc18qsj2"
            };

	        MessageText.Text = "Collecting info...";
	        await Task.Delay(1000);
            var status = await appConnection.OpenAsync();
            if(status != AppServiceConnectionStatus.Success)
            {
	            MessageText.Text = status.ToString();
                return;
            }

            var vs = new ValueSet();
            vs.Add("action", "getimudata");
            var r = await appConnection.SendMessageAsync(vs);

            if(r.Status == AppServiceResponseStatus.Success)
            {
                UpdateUI(r);
            }

            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Foundation.Collections;
using Windows.Storage;

namespace SparkfunIoTApp
{
    class SparkfunIoTSample
    {
        public const string SampleBackgroundTaskEntryPoint = "Tasks.ImuBackgroundTask";
        public const string SampleBackgroundTaskName = "ImuBackgroundTask";
        public static string SampleBackgroundTaskProgress = "";
        public static bool SampleBackgroundTaskRegistered = false;

        public static BackgroundTaskRegistration RegisterBackgroundTask(string taskEntryPoint, string name, IBackgroundTrigger trigger, IBackgroundCondition condition)
        {
            if (TaskRequiresBackgroundAccess(name))
            {
                var requestTask = BackgroundExecutionManager.RequestAccessAsync();
            }

            var builder = new BackgroundTaskBuilder();

            builder.Name = name;
            builder.TaskEntryPoint = taskEntryPoint;
            builder.SetTrigger(trigger);

            if (condition != null)
            {
                builder.AddCondition(condition);

                builder.CancelOnConditionLoss = true;
            }

            BackgroundTaskRegistration task = null;

            try
            {
                task = builder.Register();

                UpdateBackgroundTaskRegistrationStatus(name, true);

                var settings = ApplicationData.Current.LocalSettings;

                settings.Values.Remove(name);
            }
            catch (Exception ex)
            {

            }

            return task;
        }

        public static void UnregisterBackgroundTasks(string name)
        {
            foreach (var cur in BackgroundTaskRegistration.AllTasks)
            {
                if (cur.Value.Name == name)
                {
                    cur.Value.Unregister(true);
                }
            }

            UpdateBackgroundTaskRegistrationStatus(name, false);
        }

        public static void UpdateBackgroundTaskRegistrationStatus(string name, bool registered)
        {
            switch (name)
            {
                case SampleBackgroundTaskName:
                    SampleBackgroundTaskRegistered = registered;
                    break;
            }
        }

        public static bool TaskRequiresBackgroundAccess(string name)
        {
            if (name == SampleBackgroundTaskName)
            {
                return true;
            }

            return false;
        }

        public static string GetBackgroundTaskStatus(string name)
        {
            var registered = false;
            switch (name)
            {
                case SampleBackgroundTaskName:
                    registered = SampleBackgroundTaskRegistered;
                    break;
            }

            var status = registered ? "Registered" : "Unregistered";

            object taskStatus;
            var settings = ApplicationData.Current.LocalSettings;
            if (settings.Values.TryGetValue(name, out taskStatus))
            {
                status += " - " + taskStatus;
            }
            return status;
        }

    }
}
